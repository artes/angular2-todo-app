/* tslint:disable:no-unused-variable */

import {TestBed, async, inject} from '@angular/core/testing';
import {Todo} from './todo';
import {TodoDataService} from './todo-data.service';

describe('TodoDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TodoDataService]
    });
  });

  it('should ...', inject([TodoDataService], (service: TodoDataService) => {
    expect(service).toBeTruthy();
  }));

  describe('#getAllTodos()', () => {
    it('should return an empty array by default', inject([TodoDataService], (service: TodoDataService) => {
      expect(service.getAllTodos()).toEqual([]);
    }));

    it('should return all todos', inject([TodoDataService], (service: TodoDataService) => {
      let todo1 = new Todo({
        title: "Some title 1",
        complete: true
      });
      let todo2 = new Todo({
        title: "Some title2",
      });
      service.addTodo(todo1);
      service.addTodo(todo2);
      expect(service.getAllTodos()).toEqual([todo1, todo2])
    }));

    describe('#save(todo)', () => {
      it('should automatically assign an incrementing id', inject([TodoDataService], (service: TodoDataService) => {
        let todo1 = new Todo({
          title: "Some title 1",
          complete: true
        });
        let todo2 = new Todo({
          title: "Some title2",
        });
        service.addTodo(todo1);
        service.addTodo(todo2);

        expect(service.getTodoById(1)).toEqual(todo1);
        expect(service.getTodoById(2)).toEqual(todo2);
      }));

      describe("#deleteTodoById(id)", () => {
        it("should remove todo with corresponding id", inject([TodoDataService], (service: TodoDataService) => {
          let todo1 = new Todo({
            title: "Some title 1",
            complete: true
          });
          let todo2 = new Todo({
            title: "Some title2",
          });

          service.addTodo(todo1);
          service.addTodo(todo2);
          expect(service.getAllTodos()).toEqual([todo1, todo2]);
          service.deleteTodoById(1);
          expect(service.getAllTodos()).toEqual([todo2]);
          service.deleteTodoById(2);
          expect(service.getAllTodos()).toEqual([])
        }));

        it("should not remove anything if todo with corresponding id not found", inject([TodoDataService], (service: TodoDataService) => {
          let todo1 = new Todo({
            title: "Some title 1",
            complete: true
          });
          let todo2 = new Todo({
            title: "Some title2",
          });

          service.addTodo(todo1);
          service.addTodo(todo2);
          expect(service.getAllTodos()).toEqual([todo1, todo2]);
          service.deleteTodoById(3);
          expect(service.getAllTodos()).toEqual([todo1, todo2])

        }));

        describe("#updateTodoById(id)", ()=>{
          it("should update existing todo", inject([TodoDataService], (service:TodoDataService) => {
            let values_to_update = {
              title: "Different value",
              complete: false
            };
            let todo3 = new Todo(values_to_update);
            service.addTodo(todo3);

            expect(service.updateTodoById(1, values_to_update)).toEqual(todo3)

          }));


          it("should return null if todo with corresponding id is not found", inject([TodoDataService], (service: TodoDataService) => {

            let todo = new Todo({title: 'Hello 1', complete: false});
            service.addTodo(todo);

            expect(service.updateTodoById(3, {})).toBeNull()
          }));

        });

        describe("#toggleTodoComplete(todo)", ()=>{
          it("should return the updated todo with inverse complete status", inject([TodoDataService], (service:TodoDataService)=>{
            let todo1 = new Todo({ title: "Some title 1",
                                   complete: true });

            let todo1_completed = todo1;
            todo1_completed.complete = !todo1_completed.complete;
            service.addTodo(todo1);
            expect(service.toggleTodoComplete(todo1)).toEqual(todo1_completed)
          }));
        })

      });



    })


  });



});
