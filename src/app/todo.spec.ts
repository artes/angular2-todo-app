import {Todo} from './todo';

describe('Todo', () => {
    it('should create an instance', () => {
      expect(new Todo()).toBeTruthy();
    });

    it('should accept values in constructor', () => {
      let todo = new Todo({
        title: 'First',
        description: 'Some text for the first TODO',
          complete: true

      });
        expect(todo.title).toEqual('First');
        expect(todo.description).toEqual('Some text for the first TODO');
        expect(todo.complete).toEqual(true);
    })
});
