/* tslint:disable:no-unused-variable */
"use strict";
var testing_1 = require('@angular/core/testing');
var todo_data_service_1 = require('./todo-data.service');
describe('TodoDataService', function () {
    beforeEach(function () {
        testing_1.TestBed.configureTestingModule({
            providers: [todo_data_service_1.TodoDataService]
        });
    });
    it('should ...', testing_1.inject([todo_data_service_1.TodoDataService], function (service) {
        expect(service).toBeTruthy();
    }));
    describe('#getAllTodos()', function () {
        it('should return an empty array by default', testing_1.inject([todo_data_service_1.TodoDataService], function (service) {
            expect(service.getAllTodos()).toEqual([]);
        }));
    });
});
//# sourceMappingURL=todo-data.service.spec.js.map